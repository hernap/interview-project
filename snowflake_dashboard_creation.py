# Databricks notebook source
# MAGIC %md
# MAGIC # This notebook pulls the table views down from Snowflake into Databricks to create visualizations and dashboards.
# MAGIC 
# MAGIC ### Views pulled:
# MAGIC * [Airline Flight Totals Monthly](https://community.cloud.databricks.com/?o=4171044846495539#notebook/3913118594169223/command/3913118594169225)
# MAGIC * [Airport Flight Totals Monthly](https://community.cloud.databricks.com/?o=4171044846495539#notebook/3913118594169223/command/3913118594169226)
# MAGIC * [Airline On-Time Percentage](https://community.cloud.databricks.com/?o=4171044846495539#notebook/3913118594169223/command/3913118594169227)
# MAGIC * [Airline Delay Totals](https://community.cloud.databricks.com/?o=4171044846495539#notebook/3913118594169223/command/3913118594169229)
# MAGIC * [Airport Cancellation Reasons](https://community.cloud.databricks.com/?o=4171044846495539#notebook/3913118594169223/command/3913118594169230)
# MAGIC * [Airport Delay Reasons](https://community.cloud.databricks.com/?o=4171044846495539#notebook/3913118594169223/command/3913118594169231)
# MAGIC * [Unique Routes by Airline](https://community.cloud.databricks.com/?o=4171044846495539#notebook/3913118594169223/command/3913118594169233)

# COMMAND ----------

# DBTITLE 1,Library Import and Snowflake Option Set-up
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import *
from pyspark import SparkConf, SparkContext
import pandas as pd
import databricks.koalas as ks

sfOptions = {
  "sfUrl": "https://PK30088.central-us.azure.snowflakecomputing.com", # URL
  "sfUser": "HERNAP", # User
  "sfPassword": "HernSnowflake2", # Password
  "sfDatabase": "USER_ANDY", # Snowflake Database
  "sfSchema": "PUBLIC", # Snowflake Database Schema
  "sfWarehouse": "INTERVIEW_WH" # Snowflake Warehouse Compute
}

snowflake_source = "net.snowflake.spark.snowflake"

# COMMAND ----------

# DBTITLE 1,AIRLINE FLIGHTS MONTHLY
airline_flights_monthly_df = spark.read.format(snowflake_source) \
  .options(**sfOptions) \
  .option("query", "select * from AIRLINE_FLIGHTS_MONTHLY") \
  .load()

airline_flights_monthly_df.display()

# COMMAND ----------

# DBTITLE 1,AIRPORT FLIGHTS MONTHLY
airport_flights_monthly_df = spark.read.format(snowflake_source) \
  .options(**sfOptions) \
  .option("query", "select * from AIRPORT_FLIGHTS_MONTHLY") \
  .load()

airport_flights_monthly_df.display()

# COMMAND ----------

# DBTITLE 1,AIRLINE ON-TIME PERCENTAGE
airline_on_time_df = spark.read.format(snowflake_source) \
  .options(**sfOptions) \
  .option("query", "select * from AIRLINE_ON_TIME_PCT") \
  .load()

airline_on_time_df.display()

# COMMAND ----------

# DBTITLE 1,AIRLINE DELAY TOTALS
airline_delay_totals_df = spark.read.format(snowflake_source) \
  .options(**sfOptions) \
  .option("query", "select * from AIRLINE_DELAY_TOTALS") \
  .load()

airline_delay_totals_df.display()

# COMMAND ----------

# DBTITLE 1,AIRPORT CANCELLATION REASONS
airport_cancellation_df = spark.read.format(snowflake_source) \
  .options(**sfOptions) \
  .option("query", "select * from AIRPORT_CANCELLATIONS order by AIRPORT asc") \
  .load()

airport_cancellation_df.display()

# COMMAND ----------

# DBTITLE 1,AIRPORT DELAY REASONS
airport_delays_df = spark.read.format(snowflake_source) \
  .options(**sfOptions) \
  .option("query", "select * from AIRPORT_DELAYS order by AIRPORT asc") \
  .load()

airport_delays_df.display()

# COMMAND ----------

# DBTITLE 1,MOST UNIQUE ROUTES BY AIRLINE
airline_unique_routes_df = spark.read.format(snowflake_source) \
  .options(**sfOptions) \
  .option("query", "select * from AIRLINE_UNIQUE_ROUTES") \
  .load()

airline_unique_routes_df.display()
