--- *TOTAL NUMBER OF FLIGHTS PER MONTH BY AIRLINE* ---
create or replace view AIRLINE_FLIGHTS_MONTHLY as
select
    f.AIRLINE_CODE,
    a.AIRLINE,
    f.YEAR,
    f.MONTH,
    count(distinct f.FLIGHT_ID) FLIGHT_TOTALS
from FLIGHTS f
    inner join AIRLINES a on f.AIRLINE_CODE = a.AIRLINE_CODE
group by
    f.AIRLINE_CODE,
    a.AIRLINE,
    f.YEAR,
    f.MONTH ;
    
--- *TOTAL NUMBER OF FLIGHTS PER MONTH BY AIRPORT* ---
create or replace view AIRPORT_FLIGHTS_MONTHLY as
with
airport_flights as (
  select
      f.ORIGIN_AIRPORT_CODE AIRPORT_CODE,
      o.AIRPORT,
      f.YEAR,
      f.MONTH,
      count(distinct f.FLIGHT_ID) FLIGHTS
  from FLIGHTS f
      inner join AIRPORTS o on f.ORIGIN_AIRPORT_CODE = o.AIRPORT_CODE
  group by
      f.ORIGIN_AIRPORT_CODE,
      o.AIRPORT,
      f.YEAR,
      f.MONTH

  union

  select
      f.DESTINATION_AIRPORT_CODE AIRPORT_CODE,
      d.AIRPORT,
      f.YEAR,
      f.MONTH,
      count(distinct f.FLIGHT_ID) FLIGHTS
  from FLIGHTS f
      inner join AIRPORTS d on f.DESTINATION_AIRPORT_CODE = d.AIRPORT_CODE
  group by
      f.DESTINATION_AIRPORT_CODE,
      d.AIRPORT,
      f.YEAR,
      f.MONTH
)
select
    AIRPORT_CODE,
    AIRPORT,
    YEAR,
    MONTH,
    sum(FLIGHTS) FLIGHT_TOTALS
from airport_flights
group by
    AIRPORT_CODE,
    AIRPORT,
    YEAR,
    MONTH ;
    
--- *ON TIME PERCENTAGE OF AIRLINES FOR THE YEAR OF 2015* ---
create or replace view AIRLINE_ON_TIME_PCT as
select
    f.AIRLINE_CODE,
    a.AIRLINE,
    count(distinct case when f.ARRIVAL_DELAY <= 0 then f.FLIGHT_ID else null end) ON_TIME,
    count(distinct f.FLIGHT_ID) FLIGHTS, -- Total flights includes diversions and cancellations, which are NULL for ARRIVAL_DELAY
    count(distinct case when f.ARRIVAL_DELAY <= 0 then f.FLIGHT_ID else null end) / count(distinct f.FLIGHT_ID) ON_TIME_PERCENTAGE
from FLIGHTS f
    inner join AIRLINES a on f.AIRLINE_CODE = a.AIRLINE_CODE
where 1=1
    and f.YEAR = 2015 -- There seems to be only 2015 data in the flights table but might as well be specific
group by
    f.AIRLINE_CODE,
    a.AIRLINE ;
    
--- *AIRLINES WITH THE MOST DELAYS* ---
create or replace view AIRLINE_DELAY_TOTALS as
select
    f.AIRLINE_CODE,
    a.AIRLINE,
    count(distinct case when f.DEPARTURE_DELAY > 0 then f.FLIGHT_ID else null end) DEPARTURE_DELAYS,
    count(distinct f.FLIGHT_ID) FLIGHTS,
    count(distinct case when f.DEPARTURE_DELAY > 0 then f.FLIGHT_ID else null end) / count(distinct f.FLIGHT_ID) DELAY_PERCENTAGE
from FLIGHTS f
    inner join AIRLINES a on f.AIRLINE_CODE = a.AIRLINE_CODE
group by
    f.AIRLINE_CODE,
    a.AIRLINE ;
    
--- *CANCELLATION REASONS BY AIRPORT* ---
create or replace view AIRPORT_CANCELLATIONS as
select
    f.ORIGIN_AIRPORT_CODE AIRPORT_CODE,
    o.AIRPORT,
    sum(case when f.CANCELLATION_REASON = 'A' then 1 else 0 end) CANCELLED_BY_AIRLINE,
    sum(case when f.CANCELLATION_REASON = 'B' then 1 else 0 end) WEATHER_CANCELLATION,
    sum(case when f.CANCELLATION_REASON = 'C' then 1 else 0 end) CANCELLED_BY_NATIONALAIR,
    sum(case when f.CANCELLATION_REASON = 'D' then 1 else 0 end) SECURITY_CANCELLATION,
    count(distinct f.FLIGHT_ID) FLIGHT_TOTALS
from FLIGHTS f
    inner join AIRPORTS o on f.ORIGIN_AIRPORT_CODE = o.AIRPORT_CODE
group by
    f.ORIGIN_AIRPORT_CODE,
    o.AIRPORT ;

--- *DELAY REASONS BY AIRPORT* ---
create or replace view AIRPORT_DELAYS as
select
    f.ORIGIN_AIRPORT_CODE AIRPORT_CODE,
    o.AIRPORT,
    sum(case when f.AIRLINE_DELAY > 0 then 1 else 0 end) AIRLINE_DELAY,
    sum(case when f.AIR_SYSTEM_DELAY > 0 then 1 else 0 end) AIR_SYSTEM_DELAY,
    sum(case when f.SECURITY_DELAY > 0 then 1 else 0 end) SECURITY_DELAY,
    sum(case when f.WEATHER_DELAY > 0 then 1 else 0 end) WEATHER_DELAY,
    sum(case when f.LATE_AIRCRAFT_DELAY > 0 then 1 else 0 end) LATE_AIRCRAFT_DELAY,
    count(distinct f.FLIGHT_ID) FLIGHT_TOTALS
from FLIGHTS f
    inner join AIRPORTS o on f.ORIGIN_AIRPORT_CODE = o.AIRPORT_CODE
group by
    f.ORIGIN_AIRPORT_CODE,
    o.AIRPORT ;

--- *MOST UNIQUE ROUTES BY AIRLINE* ---
create or replace view AIRLINE_UNIQUE_ROUTES as
select
    f.AIRLINE_CODE,
    a.AIRLINE,
    count(distinct concat(f.ORIGIN_AIRPORT_CODE, '-', f.DESTINATION_AIRPORT_CODE)) UNIQUE_ROUTES
from FLIGHTS f
    inner join AIRLINES a on f.AIRLINE_CODE = a.AIRLINE_CODE
group by
    f.AIRLINE_CODE,
    a.AIRLINE ;