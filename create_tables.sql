create table USER_ANDY.PUBLIC.FLIGHTS_IMPORT (
  YEAR integer,
  MONTH integer,
  DAY integer,
  DAY_OF_WEEK integer,
  AIRLINE varchar(255),
  FLIGHT_NUMBER varchar(255),
  TAIL_NUMBER varchar(255),
  ORIGIN_AIRPORT varchar(255),
  DESTINATION_AIRPORT varchar(255),
  SCHEDULED_DEPARTURE varchar(255),
  DEPARTURE_TIME varchar(255),
  DEPARTURE_DELAY integer,
  TAXI_OUT integer,
  WHEELS_OFF varchar(255),
  SCHEDULED_TIME integer,
  ELAPSED_TIME integer,
  AIR_TIME integer,
  DISTANCE integer,
  WHEELS_ON varchar(255),
  TAXI_IN integer,
  SCHEDULED_ARRIVAL integer,
  ARRIVAL_TIME varchar(255),
  ARRIVAL_DELAY integer,
  DIVERTED integer,
  CANCELLED integer,
  CANCELLATION_REASON varchar(255),
  AIR_SYSTEM_DELAY integer,
  SECURITY_DELAY integer,
  AIRLINE_DELAY integer,
  LATE_AIRCRAFT_DELAY integer,
  WEATHER_DELAY integer
) ;

create table USER_ANDY.PUBLIC.FLIGHTS as (
    select
        concat(YEAR, MONTH, DAY, DAY_OF_WEEK, '-', FLIGHT_NUMBER, '-', coalesce(TAIL_NUMBER, 'UK'), '-', ORIGIN_AIRPORT, DESTINATION_AIRPORT) FLIGHT_ID,
           *
    from FLIGHTS_IMPORT
) ;

create table USER_ANDY.PUBLIC.AIRLINES (
  IATA_CODE varchar(255),
  AIRLINE varchar(255)
) ;

create table USER_ANDY.PUBLIC.AIRPORTS (
  IATA_CODE varchar(255),
  AIRPORT varchar(255),
  CITY varchar(255),
  STATE varchar(255),
  COUNTRY varchar(255),
  LATITUDE number(38,5),
  LONGITUDE number(38,5)
) ;