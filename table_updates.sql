alter table AIRLINES rename column IATA_CODE to AIRLINE_CODE ;

alter table AIRPORTS rename column IATA_CODE to AIRPORT_CODE ;

alter table FLIGHTS rename column AIRLINE to AIRLINE_CODE ;

alter table FLIGHTS rename column ORIGIN_AIRPORT to ORIGIN_AIRPORT_CODE ;

alter table FLIGHTS rename column DESTINATION_AIRPORT to DESTINATION_AIRPORT_CODE ;