# Databricks notebook source
# MAGIC %md
# MAGIC ## This notebook contains the import process for the AIRLINES, AIRPORTS, and FLIGHTS data from the shared Google Drive [Link](https://drive.google.com/drive/folders/18Mkt2Ku3gIxenT-zjYi68kcufpcvNwbv)
# MAGIC 
# MAGIC ### Steps:
# MAGIC 1. Connection to Snowflake Warehouse
# MAGIC 2. Import of airlines.csv to AIRLINES table
# MAGIC 3. Import of airports.csv to AIRPORTS table
# MAGIC 4. Import of flights folder partition-0X.csv into the FLIGHTS_IMPORT table

# COMMAND ----------

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import *
from pyspark import SparkConf, SparkContext
import pandas as pd
import databricks.koalas as ks

sfOptions = {
  "sfUrl": "https://PK30088.central-us.azure.snowflakecomputing.com", # URL
  "sfUser": "HERNAP", # User
  "sfPassword": "HernSnowflake2", # Password
  "sfDatabase": "USER_ANDY", # Snowflake Database
  "sfSchema": "PUBLIC", # Snowflake Database Schema
  "sfWarehouse": "INTERVIEW_WH" # Snowflake Warehouse Compute
}

snowflake_source = "net.snowflake.spark.snowflake"


# COMMAND ----------

# DBTITLE 1,AIRLINES - Import from Google Drive into a Spark DataFrame
airlines_url = 'https://drive.google.com/file/d/1tlwiaK74rmUlqxZocyZU_eFsHMnaS1L2/view?usp=sharing'
airlines_path = 'https://drive.google.com/uc?export=download&id=' + airlines_url.split('/')[-2]
airlines_df = pd.read_csv(airlines_path)
airlines_spark_df = spark.createDataFrame(airlines_df)

# COMMAND ----------

# DBTITLE 1,AIRLINES - Write to the created AIRLINES Snowflake table
airlines_spark_df.write \
  .format("snowflake") \
  .options(**sfOptions) \
  .option("dbtable", "AIRLINES") \
  .mode("append") \
  .save()

# COMMAND ----------

# DBTITLE 1,AIRPORTS - Import from Google Drive into a Spark DataFrame
airports_url = 'https://drive.google.com/file/d/1vHhDvqMacy7eEPdv4kdXDXvbrcx3-oz8/view?usp=sharing'
airports_path = 'https://drive.google.com/uc?export=download&id=' + airports_url.split('/')[-2]
airports_df = pd.read_csv(airports_path)
airports_spark_df = spark.createDataFrame(airports_df)

# COMMAND ----------

# DBTITLE 1,AIRPORTS - Write to the created AIRPORTS Snowflake table
airports_spark_df.write \
  .format("snowflake") \
  .options(**sfOptions) \
  .option("dbtable", "AIRPORTS") \
  .mode("append") \
  .save()

# COMMAND ----------

# DBTITLE 1,FLIGHTS - URLs
# 'https://drive.google.com/file/d/1MllabRPjjqx2ysZhj9wQySvX5PazkXGV/view?usp=sharing'
# 'https://drive.google.com/file/d/1QFRheI8EO0NV9SorTg2AZiLa1RkqQmuu/view?usp=sharing'
# 'https://drive.google.com/file/d/1CMN4UmI1-OHv96nZDwvas2Ku9v4OTC__/view?usp=sharing'
# 'https://drive.google.com/file/d/1CV-zqFSqU0z-GrdoRMYhAmTprjQ5vHc-/view?usp=sharing'
# 'https://drive.google.com/file/d/1L7IBJeuDdvMNkOR96O3Jq-qacMNrGuoy/view?usp=sharing'
# 'https://drive.google.com/file/d/1aHEx-gUD4tJgV4Nz-Dqh0WTHvJH9DCw1/view?usp=sharing'
# 'https://drive.google.com/file/d/1VTlw_j3J24A5J5yudtxD4oW0VAX6HSTO/view?usp=sharing'
# 'https://drive.google.com/file/d/1hqUEYdP4w6sF4KhXVaYGSIMsaQl-Z0Xx/view?usp=sharing'

# COMMAND ----------

# DBTITLE 1,FLIGHTS - Create URL list
flight_URLs = [
  'https://drive.google.com/file/d/1MllabRPjjqx2ysZhj9wQySvX5PazkXGV/view?usp=sharing',
  'https://drive.google.com/file/d/1QFRheI8EO0NV9SorTg2AZiLa1RkqQmuu/view?usp=sharing',
  'https://drive.google.com/file/d/1CMN4UmI1-OHv96nZDwvas2Ku9v4OTC__/view?usp=sharing',
  'https://drive.google.com/file/d/1CV-zqFSqU0z-GrdoRMYhAmTprjQ5vHc-/view?usp=sharing',
  'https://drive.google.com/file/d/1L7IBJeuDdvMNkOR96O3Jq-qacMNrGuoy/view?usp=sharing',
  'https://drive.google.com/file/d/1aHEx-gUD4tJgV4Nz-Dqh0WTHvJH9DCw1/view?usp=sharing',
  'https://drive.google.com/file/d/1VTlw_j3J24A5J5yudtxD4oW0VAX6HSTO/view?usp=sharing',
  'https://drive.google.com/file/d/1hqUEYdP4w6sF4KhXVaYGSIMsaQl-Z0Xx/view?usp=sharing'
]

# COMMAND ----------

# DBTITLE 1,FLIGHTS - Loop through URL list and create new list with download paths
flight_URL_DLs = []

for i in flight_URLs:
  path = 'https://drive.google.com/uc?export=download&id=' + i.split('/')[-2]
  flight_URL_DLs.append(path)

# COMMAND ----------

# DBTITLE 1,FLIGHTS - Check output of download path list
flight_URL_DLs

# COMMAND ----------

# DBTITLE 1,FLIGHTS - Loop through download path list and create full Pandas DataFrame from list
flights_csv = []

for i in flight_URL_DLs:
  flights_csv.append(pd.read_csv(i))

flights_test_df = pd.concat(flights_csv)

# COMMAND ----------

# DBTITLE 1,FLIGHTS - Convert Pandas DataFrame to Spark
spark_flights_df = spark.createDataFrame(flights_test_df)

# COMMAND ----------

# DBTITLE 1,FLIGHTS - Write to the created FLIGHTS_IMPORT Snowflake table
spark_flights_df.write \
  .format("snowflake") \
  .options(**sfOptions) \
  .option("dbtable", "FLIGHTS_IMPORT") \
  .mode("append") \
  .save()
